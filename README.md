# RAML API DESIGNER

Designing API's with [RAML](https://raml.org/) specification using the
[Api Designer](https://github.com/mulesoft/api-designer) tool.


## 1. How to Use

### 1.1 Clone Api Specification Repositories

```bash
./raml clone https://github.com/raml-org/raml-examples.git
```

or with a custom folder name...

```bash
./raml clone "https://github.com/raml-org/raml-examples.git api-specification"
```

### 1.2. RAML Editor

By default the editor will run in http://localhost:3000 and is Api Mock Server
will run in http://localhost:3800

#### Running detached from terminal

```bash
./raml -d editor ./raml-examples/others/mobile-order-api/api.raml
```

or attached to the terminal

```bash
./raml editor ./raml-examples/others/mobile-order-api/api.raml
```

Visit now http://localhost:3000

> **NOTE**:
>
> * In the RAML example the `baseUri` is http://localhost:8081/api, thus not
>   matching the default port `3800` of our Api Mock Server, therefore in the
>   *RAML Editor* tab **Try It** we will not be able to reach the mock server.
>
> * Please proceed to next step to be able to use the **Try It** functionality.
>


#### Running with custom editor and api mock server ports

```bash
./raml -d --editor-port 3001 --api-mock-port 8081 editor ./raml-examples/others/mobile-order-api/api.raml
```

or

```bash
./raml -d --ep 3001 --amp 8081 editor ./raml-examples/others/mobile-order-api/api.raml
```

Visit now http://localhost:3001 and now we can use the **Try It** functionality.


### 1.3. Standalone API Mock Server

By default runs on http://localhost:3900

#### Running attached to the terminal

```bash
./raml mock ./raml-examples/others/mobile-order-api/api.raml
```

Visit now http://localhost:3900/api/orders?userId=1964401a-a8b3-40c1-b86e-d8b9f75b5842


#### Running detached from terminal

```bash
./raml -d mock ./raml-examples/others/mobile-order-api/api.raml
```

Visit now http://localhost:3900/api/orders?userId=1964401a-a8b3-40c1-b86e-d8b9f75b5842


#### Running on a custom port

```bash
./raml -d --amp 3901 mock ./raml-examples/others/mobile-order-api/api.raml
```

Visit now http://localhost:3901/api/orders?userId=1964401a-a8b3-40c1-b86e-d8b9f75b5842


### 1.4. Validating a RAML Api Specification


#### On Success

```bash
$ ./raml validate ./raml-examples/others/mobile-order-api/api.raml
Validating /home/node/api-specification/api.raml...
Valid!
```

#### On Error

This error is due to a typo in a field type, so instead of `string` it as `strin`.

```bash
$ ./raml validate ./raml-examples/others/mobile-order-api/api.raml
Validating /home/node/api-specification/api.raml...
{
  "parserErrors": [
    {
      "code": "INHERITING_UNKNOWN_TYPE",
      "message": "Inheriting from unknown type",
      "path": "api.raml",
      "range": {
        "start": {
          "line": 16,
          "column": 8,
          "position": 347
        },
        "end": {
          "line": 16,
          "column": 12,
          "position": 351
        }
      },
      "isWarning": false
    }
  ]
}
```


#### On Exception

Does not find the file we gave to be validated.

```bash
$ ./raml validate ./raml-examples/others/mobile-order-api/api.raml
Creating network "apidesigner_raml_network" with driver "bridge"
Validating api.raml...
{
  "name": "YAMLException",
  "reason": "Error: ENOENT: no such file or directory, open '/home/node/api.raml'",
  "mark": null,
  "message": "JS-YAML: Error: ENOENT: no such file or directory, open '/home/node/api.raml'",
  "isWarning": false
}
```
